# Test Script

## Goal

Write a program that prints all the numbers from 1 to 100. However, for multiples of 3, instead of the number, print "Foo". For multiples of 5 print "Bar". For numbers which are multiples of both 3 and 5, print "FooBar".

But here's the catch: you can use only one if. No multiple branches, ternary operators or else.

## Requirements

1 if
You can't use else, else if or ternary

## Install the project

Use the command `npm i` to install the project and dependencies.

## Launch unit test

Use the command `npm run test` to launch the unit test.
