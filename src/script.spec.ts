import { main } from "./script";
import { expect } from "chai";
import "mocha";

describe("Main function", () => {
  it("should return foo or/and bar for multiple of 3 and 5", () => {
    const results = main(100);

    // Check the number result
    const checkMultiple = (number, result) => {
      if (number % 3 === 0 && number % 5 === 0) {
        expect(result).to.equal("FooBar");
      } else if (number % 3 === 0) {
        expect(result).to.equal("Foo");
      } else if (number % 5 === 0) {
        expect(result).to.equal("Bar");
      }
    };

    expect(results.length).to.equal(100);

    results.forEach((result, number) => {
      checkMultiple(number + 1, result);
    });
  });
});
