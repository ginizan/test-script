/**
 * Interface used to map a multiple of a number to a string
 */
interface MultipleToStringInterface {
  multiple: number;
  string: string;
}

/**
 * Replace 'number' to a string if 'number' is a multiple of one number of the list 'multipleToString'
 * @param number
 * @param multipleToString
 */
const replaceMultipleToString = (
  number: number,
  multipleToStringList: MultipleToStringInterface[]
): string => {
  let result: (string | number)[] = [number];

  let multipleFind = 0;
  multipleToStringList.forEach(multipleToString => {
    // Unique If, that check if the number is a multiple
    if (number % multipleToString.multiple === 0) {
      result[multipleFind] = multipleToString.string;
      multipleFind++;
    }
  });

  // Print the result
  console.log(result.join(""));

  return result.join("");
};

/**
 * Create an array of number from 1 to max
 */
const createArray = (max: number): number[] => {
  // Create an array from 0 to 100
  const array = [...Array(max + 1).keys()];

  // Remove the 0
  array.shift();

  return array;
};

/**
 * Main function
 * @param max
 */
export const main = (max: number): string[] => {
  // Pre-config the replace function with the MutipleToString list
  const replace = number =>
    replaceMultipleToString(number, [
      { multiple: 3, string: "Foo" },
      { multiple: 5, string: "Bar" }
    ]);

  // Create an array of number from 1 to max
  const numbers = createArray(max);

  return numbers.map(replace);
};
